#!/usr/bin/env python
import rospy
from state_msg_to_csv.msg import state as CFState
import csv
import time
import sys,os
pathname = os.path.dirname(os.path.dirname(sys.argv[0]))

class CSVWriter:
    def __init__(self,topic_name='state_cf'):
        self.sub_state = rospy.Subscriber(topic_name, CFState, self.msgReceived)
        if not os.path.isdir(pathname+"/logdata"):
            os.makedirs(pathname+"/logdata")
        if not os.path.isdir(pathname+"/logdata/"+time.strftime("%Y%m%d")):
            os.makedirs(pathname+"/logdata/"+time.strftime("%Y%m%d"))
        self.file_name = pathname+"/logdata/"+time.strftime("%Y%m%d")+"/"+time.strftime("%Y%m%d-%H%M")+"_"+topic_name+".csv"
        self.start = None
        self.f = open(self.file_name, 'wb')
        self.writer = csv.writer(self.f)
        self.writer.writerow("time,x,y,z,yaw".split(','))
            
    def msgReceived(self, msg):
        t = msg.header.stamp.to_time()
        if self.start == None:
            self.start = t 
        try:
            data = '%.9f,%f,%f,%f,%f' % (t-self.start, msg.x, msg.y, msg.z, msg.yaw)
            self.writer.writerow(data.split(','))
        except csv.Error as e:
            sys.exit('file %s, line %d: %s' % (file_name, writer.line_num, e))

    
    
if __name__ == '__main__':
    rospy.init_node('csv_writer')
    state_goal_writer = CSVWriter('state_goal')
    state_cf_writer = CSVWriter('state_cf')
    state_cferr_writer = CSVWriter('state_cferr')
    rospy.spin()
