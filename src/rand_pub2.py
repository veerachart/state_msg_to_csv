#!/usr/bin/env python
import rospy
from beginner_tutorials.msg import state as CFState
import math
from math import pi as PI

def rand_pub():
    pub = rospy.Publisher('state_goal', CFState)
    rospy.init_node('rand_publisher2')
    i = 1
    msg = CFState()
    start = rospy.get_time()
    while not rospy.is_shutdown():
        msg.header.stamp = rospy.Time.now()
        t = rospy.get_time() - start
        msg.x = 2*math.cos(2*PI*t)
        msg.y = 4*math.sin(0.5*PI*t)
        msg.z = 0
        msg.yaw = (round(10*t))%360
        pub.publish(msg)
        rospy.sleep(0.1)


if __name__ == '__main__':
    try:
        rand_pub()
    except rospy.ROSInterruptException:
        pass
