state_msg_to_csv package

To be used with ROS

How to use:
1. Extract this into src directory of your catkin workspace (e.g. catkin_ws directory)
2. Run
   $ catkin_make
   in your catkin workspace
3. Run nodes rand_pub.py, rand_pub2.py, and rand_pub3.py to publish 3 topics
4. Run csv_write.py node. The csv file will be stored in the directory "logdata" inside the package's directory.
